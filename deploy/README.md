# Deployment Configurations

Sub directories contain scripts and configuration for deployment in target platforms.
The Original comes from https://github.com/microservices-demo

To log into Sock Shop and complete a transaction, you'll need credentials. The 'user' microservice ships with the following accounts.

<table class="user-creds">
  <thead>
    <tr>
      <td>Username</td>
      <td>Password</td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>user</td>
      <td>password</td>
    </tr>
    <tr>
      <td>user1</td>
      <td>password</td>
    </tr>
    <tr>
      <td>Eve_Berger</td>
      <td>eve</td>
    </tr>
  </tbody>
</table>
